import {
  combineReducers
} from 'redux'
import index from './index/reducer'

const store = combineReducers({
  index,
})

export default store