export const setParamsAction = payload => {
  return {
    type: 'SET_PARAMS',
    payload
  }
}

export const addNodeAction = payload => {
  return {
    type: 'ADD_NODE',
    payload
  }
}

export const delNodeAction = payload => {
  return {
    type: 'DEL_NODE',
    payload
  }
}

export const setNodeParamsAction = payload => {
  return {
    type: 'SET_NODE_PARAMS',
    payload
  }
}

export const addCoinNodeAction = payload => {
  return {
    type: 'ADD_COIN_NODE',
    payload
  }
}

export const delCoinNodeAction = payload => {
  return {
    type: 'DEL_COIN_NODE',
    payload
  }
}

export const setCoinNodeParamsAction = payload => {
  return {
    type: 'SET_COIN_NODE_PARAMS',
    payload
  }
}

export const setCdpParamsAction = payload => {
  return {
    type: 'SET_CDP_CREATE_PARAMS',
    payload
  }
}

export const setCdpRedeemParamsAction = payload => {
  return {
    type: 'SET_CDP_REDEEM_PARAMS',
    payload
  }
}