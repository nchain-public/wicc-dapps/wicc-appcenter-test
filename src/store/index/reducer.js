import { fromJS } from "immutable";
const initState = fromJS({
  contractInvokeParams: {
    regId: "2991367-1",
    contractField: "f01234",
    inputAmount: "0.01",
    remark: "remark",
    res: null
  },
  contractIssueParams: {
    contractContent: 'mylib = require "mylib"',
    contractDesc: "contract describe"
  },
  transferParams: {
    amount: "0.01",
    collectionAddress: "WPqY8RJHN1u4Kzrnj2mHG9V8igJJVVcveb",
    remark: "remark"
  },
  uContractInvokeParams: {
    amount: "0.01",
    coinSymbol: "WICC",
    regId: "241107-3",
    contract: "f001",
    memo: "test transfer",
    genSign: 0
  },
  assetIssueParams: {
    assetSymbol: "WGTRGT",
    assetName: "GTR",
    assetSupply: "0.01",
    assetOwnerId: "0-11",
    assetMintable: "false"
  },
  cdpCreateParams: {
    assetMap: [{"bCoinToStake":"100", "coinSymbol":"WICC"}],
    scoinSymbol: "WUSD",
    scoinNum: "100",
    cdpTxID: "",
  },
  cdpRedeemParams: {
    repayNum: "10",
    assetMap: [{"redeemCoinNum":"100", "coinSymbol":"WICC"}],
    cdpTxID: "",
  },
  cdpLiquidParams: {
    assetSymbol: "WICC",
    liquidateNum: "10",
    cdpTxID: "",
  },
  dexLimitParams: {
    dexTxType : "Limit_BUY / Limit_SELL", //Limit_BUY / Limit_SELL
    coinType : "WICC",  // 币种
    assetType : "WUSD", // 资产类型
    amount : "123", //资产金额
    price : "2", // 价格*10^8
    
  },
  dexMarketParams: {
    dexTxType : "Market_BUY / Market_SELL", //Market_BUY/ Market_SELL
    coinType : "WICC",  // 币种
    assetType : "WUSD", // 资产类型
    amount : "120", //资产金额
  },
  dexCancelOrderParams: {
    dexTxNum:""//订单号
  },
  assetUpdateParams: {
    assetSymbol: "",
    updateType: "",
    updateValue: ""
  },
  uCoinTransferParams: {
    destArr: [
      {
        coinSymbol: "WICC",
        amount: "0.01",
        destAddr: "0-5"
      }
    ],
    memo: "memo",
    genSign: 0
  },
  getAddInfoParams: {},
  nodeVoteParams: [{}],
  nodeParams: null
});

const setParams = (state, payload) => {
  return state.updateIn([payload.type, payload.name], val => payload.value);
};

const addNode = (state, payload) => {
  return state.update("nodeVoteParams", arr => arr.push({}));
};

const delNode = (state, payload) => {
  return state.update("nodeVoteParams", arr => arr.remove(payload));
};

const setNodeParams = (state, payload) => {
  return state.updateIn(
    ["nodeVoteParams", payload.index, payload.name],
    val => payload.value
  );
};

const addCoinNode = (state, payload) => {
  return state.updateIn(["uCoinTransferParams", "destArr"], arr =>
    arr.push({})
  );
};

const delCoinNode = (state, payload) => {
  return state.updateIn(["uCoinTransferParams", "destArr"], arr =>
    arr.remove(payload)
  );
};

const setCoinNodeParams = (state, payload) => {
  return state.updateIn(
    ["uCoinTransferParams", "destArr", payload.index, payload.name],
    val => payload.value
  );
};
const setCdpParams = (state, payload) => {
  return state.updateIn(
    ["cdpCreateParams", "assetMap", payload.index, payload.name],
    val => payload.value
  );
};

const setCdpRedeemParams = (state, payload) => {
  return state.updateIn(
    ["cdpRedeemParams", "assetMap", payload.index, payload.name],
    val => payload.value
  );
};

const reducerMap = {
  SET_PARAMS: setParams,
  ADD_NODE: addNode,
  DEL_NODE: delNode,
  SET_NODE_PARAMS: setNodeParams,
  ADD_COIN_NODE: addCoinNode,
  DEL_COIN_NODE: delCoinNode,
  SET_COIN_NODE_PARAMS: setCoinNodeParams,
  SET_CDP_CREATE_PARAMS: setCdpParams,
  SET_CDP_REDEEM_PARAMS: setCdpRedeemParams,
};

const handlerReducer = (state, action) => {
  if (reducerMap.hasOwnProperty(action.type)) {
    return reducerMap[action.type](state, action.payload);
  }
  return state;
};

const reducer = (state = initState, action) => {
  return handlerReducer(state, action);
};

export default reducer;
