import {
  put,
  takeEvery
} from 'redux-saga/effects'
import Api from '../../Api'

function* getData(action) {
  const res = yield Api({method: 'GET', url: '/api/index/info'})
}

function* saga() {
  yield takeEvery('ADD_TODO', getData)
}

export default saga