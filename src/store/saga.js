import { all } from 'redux-saga/effects'
import indexSaga from './index/saga'

export default function* rootSaga() {
  yield all([
    indexSaga()
  ])
}