import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Index from './pages/index/Index'
import About from './pages/About'

export default class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" component={Index}></Route>
          <Route path="/about" component={About}></Route>
        </Switch>
      </Router>
    )
  }
}
