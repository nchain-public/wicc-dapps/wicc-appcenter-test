import React from 'react';
import JSONTree from 'react-json-tree'
import PropTypes from 'prop-types'
import {Input, Button} from 'antd';
import './index.scss'

class Module extends React.Component {
  constructor() {
    super()
  }

  render() {
    const {data, handleChange, res, handleSubmit, title} = this.props
    return (
      <div>
        <div className="module">
          <div className="title">{title}</div>
          {data.map((item, index) => <div className="params" key={index}>
            <div className="param">
              <label>{item.label}</label>
              <Input
                type="text"
                name={item.name}
                value={item.value}
                suffix={item.suffix}
                placeholder={item.placeholder}
                onChange={(event) => handleChange(event)}/>
            </div>
            {item.transform && <div className="param transform">
              <label></label>
              <div>{item.transform}</div>
            </div>}
          </div>)}
          {res && <JSONTree data={res} shouldExpandNode={() => true}/>}
          <div className="btn-group">
            <Button type="primary" onClick={event => handleSubmit(event)} block>Submit</Button>
          </div>
        </div>
      </div>
    )
  }
}

Module.defaultProps = {
  data: [],
  title: '',
  res: null
}

Module.propTypes = {
  data: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  handleChange: PropTypes.func,
  res: PropTypes.object,
  handleSubmit: PropTypes.func
}

export default Module