const proxy = require('http-proxy-middleware');

module.exports = function (app) {
  app.use('/api', proxy({
    target: 'https://hn-expmain.vm.wicc.me',
    changeOrigin: true,
    secure: false,
  }));
};