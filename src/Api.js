import fetch from 'cross-fetch'
export default (payload) => {
  return fetch(payload.url, {
    method: payload.method,
  }).then(res => res.data)
}